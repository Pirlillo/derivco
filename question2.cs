using System;
using System.Collections.Generic;
using System.Linq;


namespace Question2
{
    public class Card
    {
        private int _number { get; set; }
        public int Number => _number;
        private int _suit { get; set; }
        public int Suit => _number;
        public Card(int number, int suit)
        {
            _number = number;
            _suit = suit;
        }

    }

    public class WildCard:Card{
		public WildCard():base(Int32.MaxValue,Int32.MaxValue){
		}
	}

    class HighCard
    {

        private List<Card> _bigDeck = new List<Card>();
        public HighCard(int deck, int cardsPerSuit)
        {
            for (int i = 0; i < deck; i++)
            {
                for (int s = 0; s < 4; s++)
                {
                    for (int j = 1; j <= cardsPerSuit; j++)
                    {
                        _bigDeck.Add(new Card(j, s));
                    }
                }
                _bigDeck.Add(new WildCard());
            }
        }

        public HighCard() : this(1, 13)
        {

        }

        private int Compare(Card card1, Card card2)
        {
            if (card1.Number == card2.Number && card1.Suit == card2.Suit)
            {
                return 0;
            }
            if (card1.Number > card2.Number || (card1.Number == card2.Number && card1.Suit > card2.Suit))
            {
                return 1;
            }
            return -1;
        }

        public bool Play()
        {
            Random rnd = new Random();

            var tie = true;
            var win = 0;
            do
            {

                int i = rnd.Next() % _bigDeck.Count();
                int j = rnd.Next() % _bigDeck.Count();
                var card1 = _bigDeck[i];
                var card2 = _bigDeck[j];
                win = Compare(card1, card2);
            } while (win == 0);
            return win > 0;

        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            HighCard card = new HighCard(1, 20);
            var game = card.Play();
            if (game)
            {
                Console.WriteLine("win");
            }
            else
            {
                Console.WriteLine("lose");
            }
        }
    }
}
