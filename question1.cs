
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Question1
{

    public class Codec
    {
        private const int Plus = 62;
        private const int Slash = 63;
        private const int Equal = 64;
        private const char Terminate = '=';
        private char[] _transcode = new char[65];

        public Codec()
        {
            for (int i = 0; i < Plus; i++)
            {
                _transcode[i] = (char)((int)'A' + i);
                if (i > 25) _transcode[i] = (char)((int)_transcode[i] + 6);
                else if (i > 51) _transcode[i] = (char)((int)_transcode[i] - 0x4b);
            }
            _transcode[Plus] = '+';
            _transcode[Slash] = '/';
            _transcode[Equal] = '=';
        }

        public string encode(string input)
        {
            int outputLength = (int)Math.Ceiling((double)input.Length / 3) * 4;
            char[] output = new char[outputLength];

            for (int i = 0; i < outputLength; i++)
            {
                output[i] = Terminate;
            }

            int outputIndex = 0;
            int reflex = 0;
            const int s = 0x3f;

            for (int j = 0; j < input.Length; j++)
            {
                reflex <<= 8;
                reflex &= 0x00ffff00;
                reflex += input[j];

                int x = ((j % 3) + 1) * 2;
                int mask = s << x;
                while (mask >= s)
                {
                    int pivot = (reflex & mask) >> x;
                    output[outputIndex++] = _transcode[pivot];
                    reflex &= (~mask);
                    mask >>= 6;
                    x -= 6;
                }
            }

            switch (input.Length % 3)
            {
                case 1:
                    reflex <<= 4;
                    output[outputIndex++] = _transcode[reflex];
                    break;
                case 2:
                    reflex <<= 2;
                    output[outputIndex++] = _transcode[reflex];
                    break;
            }

            Console.WriteLine("{0} --> {1}\n", input, new string(output));
            return new string(output);
        }

        public string decode(string input)
        {
            int outputLength = (int)Math.Ceiling((double)input.Length / 4) * 3 + 1;
            char[] output = new char[outputLength];
            int outputIndex = 0;
            int bits = 0;
            int reflex = 0;

            for (int j = 0; j < input.Length; j++)
            {
                reflex <<= 6;
                bits += 6;
                bool fTerminate = (Terminate == input[j]);
                if (!fTerminate)
                    reflex += indexOf(input[j]);

                while (bits >= 8)
                {
                    var bitMod8 = bits % 8;
                    int mask = 0x000000ff << (bitMod8);
                    output[outputIndex++] = (char)((reflex & mask) >> (bitMod8));
                    reflex &= (~mask);
                    bits -= 8;
                }

                if (fTerminate)
                    break;
            }
            Console.WriteLine("{0} --> {1}\n", input, new string(output));
            return new string(output);
        }

        private int indexOf(char ch)
        {
            int index;
            for (index = 0; index < _transcode.Length; index++)
                if (ch == _transcode[index])
                    break;
            return index;
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            var codec = new Codec();

            string test_string = "This is a test string";
            if (String.Compare(test_string, codec.decode(codec.encode(test_string))) == 0)
            {
                Console.WriteLine("Test succeeded");
            }
            else
                Console.WriteLine("Test failed");
        }

    }
}